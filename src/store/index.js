import {
    createStore
} from 'vuex'

export default createStore({
    state: {
        sectionNumbers: [1, 2, 3, 4, 5, 6], // For displaying the section numbers
        activeSection: 0, // Set to bypass steps
        eligibilityVersion: [], // The role/version the user has picked. This sets the questions displayed ['nglp', 'ndp']
        errorState: false, // Set to true & 8 above to see error summary
        errorStateSetOnQuestionNumbers: [], // Used to set the clipboard error image and other error bits
        step: 0, // Current step. Used to check what step we are on in the 'section' counter displayed in the app
        totalSteps: 6, // The total number of steps required.
        sectionStop: true, // Not currently used
        summariesActive: false, // We check against this to teleport errors to summaries section
        debug: false, // For debugging help set to true


        sections: [
            {
                sectionNumber: 1, // For displaying section numbers
                sectionComplete: false, // For moving on to the next section
                sectionTitle: 'Your age', // For displaying section titles
                sectionIntro: '', // For displaying intro text to the section
                sectionSkipped: false, // For skipping the section (not currently used within the code)
                questions: [ // Questions for the section
                    {
                    questionNumber: 1, // Q1 // For setting properties to sections & questions
                    evQuestion: ['nglp', 'ndp'], // What eligibility version this question should be shown for
                    questionIsActive: true, // For activating the next question
                    lastQuestion: true, // For knowing when to move on to the next section
                    subQuestionRequired: false, // For displaying sub-questions when needed
                    preText: '', // Not currently used, but for adding pre text to a question
                    question: '<p>Please can you confirm you are over the age of 17 years?</p>', // The question html
                    answer: 'yes', // The answer we want
                    userAnswer: '', // The users answer
                    questionErrorSet: false, // If we need to display an error when the user answer is wrong
                    required: true, // All questions are required, so may not need this
                    questionErrorPopup: true, // If the modal is displayed on error
                    questionPopupOn: ['error'], // ['error', 'correct', 'both'] Used to activate correct time and message type to be passed - 'both' will show the error version
                    questionCorrectPopupMessage: '<p>You pressed yes</a></p>', // Pass message for the modal
                    questionErrorPopupMessage: '<p>You must be over the age of 17 years and hold an undergraduate degree to join one of our national graduate programmes. <a href="https://careers.policenow.org.uk/registeryourinterest/CreateAccount.aspx" target="_blank">Please register your interest for a future recruitment intake.</a></p>', // Error message for the modal
                    errorMessage: '<p>You must be over the age of 17 years and hold an undergraduate degree to join one of our national graduate programmes. <a href="https://careers.policenow.org.uk/registeryourinterest/CreateAccount.aspx" target="_blank">Please register your interest for a future recruitment intake.</a></p>', // Error/Help message displayed on the summary section

                    // SUB-QUESTIONS
                    // subQuestionNumber: 1, // For setting properties to sections & sub-questions
                    // subQuestionIsActive: true, // For activating the next question
                    // subPreText: '', // Not currently used, but for adding pre text to a question
                    // subQuestion: '', // The sub-question html
                    // subAnswer: 'yes', // The answer we want
                    // subUserAnswer: '', // The users answer
                    // subQuestionErrorSet: false, // If we need to display an error when the user answer is wrong
                    // subRequired: true, // All questions are required, so may not need this
                    // subQuestionErrorPopup: true, // If the modal is displayed on error
                    // subQuestionPopupOn: [], // ['error', 'correct', 'both'] Used to activate correct time and message type to be passed - 'both' will show the error version
                    // subQuestionCorrectPopupMessage: '',// Pass message for the modal
                    // subQuestionErrorPopupMessage: '', // Error message for the modal
                    // subErrorMessage: '', // Error/Help message displayed on the
                }]
            },
            {
                sectionNumber: 2,
                sectionComplete: false,
                sectionTitle: 'Your Qualifications',
                sectionIntro: '',
                sectionSkipped: false,
                questions: [{
                    questionNumber: 1, //* Q2
                    evQuestion: ['nglp', 'ndp'],
                    questionIsActive: true,
                    lastQuestion: false,
                    subQuestionRequired: false,
                    preText: '',
                    question: '<p>Do you hold at least one Level 3 qualification (e.g. A Level, BTEC National) in any subject?</p>',
                    answer: 'yes',
                    userAnswer: '',
                    questionErrorSet: false,
                    required: true,
                    questionErrorPopup: true,
                    questionPopupOn: ['error'],
                    questionCorrectPopupMessage: '',
                    questionErrorPopupMessage: '<p>You must hold at least one UK Level 3 qualification to be able to join one of our national graduate programmes.</p><p>Further information on accepted UK Level 3 qualifications can be found <a href="https://www.gov.uk/what-different-qualification-levels-mean/list-of-qualification-levels" target="_blank">here</a> and overseas equivalents <a href="https://www.gov.uk/what-different-qualification-levels-mean/compare-different-qualification-levels" target="_blank">here</a>. If you are in the process of obtaining this level of qualification then please <a href="https://careers.policenow.org.uk/registeryourinterest/CreateAccount.aspx" target="_blank">register your interest</a> for a future intake.</p><p>Alternatively, please see <a href="https://www.joiningthepolice.co.uk/application-process/ways-in-to-policing" target="_blank">here</a> for alternative careers available to you within the police.</p>',
                    errorMessage: '<p>You must hold at least one UK Level 3 qualification to be able to join one of our national graduate programmes.</p><p>Further information on accepted UK Level 3 qualifications can be found <a href="https://www.gov.uk/what-different-qualification-levels-mean/list-of-qualification-levels" target="_blank">here</a> and overseas equivalents <a href="https://www.gov.uk/what-different-qualification-levels-mean/compare-different-qualification-levels" target="_blank">here</a>. If you are in the process of obtaining this level of qualification then please <a href="https://careers.policenow.org.uk/registeryourinterest/CreateAccount.aspx." target="_blank">register your interest</a> for a future intake.</p><p>Alternatively, please see <a href="https://www.joiningthepolice.co.uk/application-process/ways-in-to-policing" target="_blank">here</a> for alternative careers available to you within the police.</p>',
                },
                {
                    questionNumber: 2, //* Q3
                    evQuestion: ['nglp', 'ndp'],
                    questionIsActive: false,
                    lastQuestion: false,
                    subQuestionRequired: false,
                    preText: '',
                    question: '<p>Have you achieved or are you working towards completing a Bachelors/Honours degree with a 2.2 or above from a UK university or equivalent from a non-UK university by the programme start date as shown above?</p>',
                    answer: 'yes',
                    userAnswer: '',
                    questionErrorSet: false,
                    required: true,
                    questionErrorPopup: true,
                    questionPopupOn: ['error'],
                    questionCorrectPopupMessage: '',
                    questionErrorPopupMessage: '<p>You must be working towards or hold a Bachelors/Honours degree (or equivalent from a non-UK university) with at least a 2.2 classification in any subject before starting one of our national graduate leadership programmes.</p><p>If you are in the process of obtaining a degree and you will not have finished this by the current intake start date then <a href="https://careers.policenow.org.uk/registeryourinterest/CreateAccount.aspx" target="_blank">please register your interest for a future programme intake</a>.</p>',
                    errorMessage: '<p>You must be working towards or hold a Bachelors/Honours degree (or equivalent from a non-UK university) with at least a 2.2 classification in any subject before starting one of our national graduate leadership programmes.</p><p>If you are in the process of obtaining a degree and you will not have finished this by the current intake start date then <a href="https://careers.policenow.org.uk/registeryourinterest/CreateAccount.aspx" target="_blank">please register your interest for a future programme intake</a>.</p>',
                },
                {
                    questionNumber: 3, //* Q4
                    evQuestion: ['nglp', 'ndp'],
                    questionIsActive: false,
                    lastQuestion: false,
                    subQuestionRequired: false,
                    preText: '',
                    question: '<p>Have you undertaken the College of Policing-licensed pre-join degree in Professional Policing Practice?</p>',
                    answer: 'no',
                    userAnswer: '',
                    questionErrorSet: false,
                    required: true,
                    questionErrorPopup: true,
                    questionPopupOn: ['error'],
                    questionCorrectPopupMessage: '',
                    questionErrorPopupMessage: '<p>Police Now\'s national graduate programmes are Degree Holder Entry routes into policing. Therefore you cannot hold a pre-join degree in Professional Policing Practice.</p>',
                    errorMessage: '<p>Police Now\'s national graduate programmes are Degree Holder Entry routes into policing. Therefore you cannot hold a pre-join degree in Professional Policing Practice.</p>',
                },
                {
                    questionNumber: 4, //* Q5
                    evQuestion: ['nglp', 'ndp'],
                    questionIsActive: false,
                    lastQuestion: true,
                    subQuestionRequired: false,
                    preText: '',
                    question: '<p>Are you studying for a degree (e.g. undergraduate, Masters, or other) which will <strong>not</strong> be completed by the programme start date as shown above?</p>',
                    answer: 'no',
                    userAnswer: '',
                    questionErrorSet: false,
                    required: true,
                    questionErrorPopup: true,
                    questionPopupOn: ['error'],
                    questionCorrectPopupMessage: '',
                    questionErrorPopupMessage: '<p>It is a requirement of the programme that all higher education is completed before the programme start date (including any part-time distanced learning you are enrolled on).</p><p>If your existing studies will <b>not</b> be completed by the programme start date then <a href="https://careers.policenow.org.uk/registeryourinterest/CreateAccount.aspx" target="_blank">please register your interest for a future programme intake</a>.</p>',
                    errorMessage: '<p>It is a requirement of the programme that all higher education is completed before the programme start date (including any part-time distanced learning you are enrolled on).</p><p>If your existing studies will <b>not</b> be completed by the programme start date then <a href="https://careers.policenow.org.uk/registeryourinterest/CreateAccount.aspx" target="_blank">please register your interest for a future programme intake</a>.</p>',
                }]
            },
            {
                sectionNumber: 3,
                sectionComplete: false,
                sectionTitle: 'Your experience',
                sectionIntro: '',
                sectionSkipped: false,
                questions: [{
                    questionNumber: 1, //* Q5
                    evQuestion: ['nglp', 'ndp'],
                    questionIsActive: true,
                    lastQuestion: true,
                    subQuestionRequired: false,
                    preText: '',
                    question: '<p>Have you previously trained, attested or been employed as a Police Constable?</p><p>This does not apply if you have been a Special Constable or PCSO.</p>',
                    answer: 'no',
                    userAnswer: '',
                    questionErrorSet: false,
                    required: true,
                    questionErrorPopup: true,
                    questionPopupOn: ['error'],
                    questionCorrectPopupMessage: '',
                    questionErrorPopupMessage: '<p>If you have previously trained, attested or been employed as a Police Constable then you are unable to join one of our national graduate programmes. Please see <a href="https://www.joiningthepolice.co.uk/application-process/ways-in-to-policing" target="_blank">here</a> for alternative careers available to you within the police.</p>',
                    errorMessage: '<p>If you have previously trained, attested or been employed as a Police Constable then you are unable to join one of our national graduate programmes. Please see <a href="https://www.joiningthepolice.co.uk/application-process/ways-in-to-policing" target="_blank">here</a> for alternative careers available to you within the police.</p>',
                }]
            },
            {
                sectionNumber: 4,
                sectionComplete: false,
                sectionTitle: 'Citizenship and residency',
                sectionIntro: '',
                sectionSkipped: false,
                questions: [{
                    questionNumber: 1, //* Q6
                    evQuestion: ['nglp', 'ndp'],
                    questionIsActive: true,
                    lastQuestion: false,
                    subQuestionRequired: true,
                    preText: '',
                    question: '<p>Are you a British citizen, or do you have the status of indefinite leave to remain and work without restriction in the UK?</p>',
                    answer: 'yes',
                    userAnswer: '',
                    questionErrorSet: false,
                    required: true,
                    questionErrorPopup: false,
                    questionPopupOn: [],
                    questionCorrectPopupMessage: '',
                    questionErrorPopupMessage: '',
                    errorMessage: '',


                    subQuestionNumber: 1, //* Q6a
                    subQuestionIsActive: true,
                    subPreText: '',
                    subQuestion: '<p>Are you currently working towards that status of indefinite leave to remain and work without restriction in the UK?</p>',
                    subAnswer: 'yes',
                    subUserAnswer: '',
                    subQuestionErrorSet: false,
                    subRequired: true,
                    subQuestionErrorPopup: true,
                    subQuestionPopupOn: ['error'],
                    subQuestionCorrectPopupMessage: '',
                    subQuestionErrorPopupMessage: '<p>To be eligible for appointment, you must be a British citizen or a citizen of a country that is a member of the European Economic Area (EEA) or Switzerland. Commonwealth citizens and foreign nationals are also eligible but only if they are resident in the UK free of restrictions. If you are a Commonwealth citizen or other foreign national, you must provide proof that you have no restrictions on your stay in the UK.</p><p>After 30 June 2021, nationals of the EEA or Switzerland will also need to provide evidence that their stay is free from restrictions.</p><p>Please apply for one of our programmes in the future once you have achieved this status. <a href="https://careers.policenow.org.uk/registeryourinterest/CreateAccount.aspx" target="_blank">You can register your interest here</a>.</p>',
                    subErrorMessage: '<p>To be eligible for appointment, you must be a British citizen or a citizen of a country that is a member of the European Economic Area (EEA) or Switzerland. Commonwealth citizens and foreign nationals are also eligible but only if they are resident in the UK free of restrictions. If you are a Commonwealth citizen or other foreign national, you must provide proof that you have no restrictions on your stay in the UK.</p><p>After 30 June 2021, nationals of the EEA or Switzerland will also need to provide evidence that their stay is free from restrictions.</p><p>Please apply for one of our programmes in the future once you have achieved this status. <a href="https://careers.policenow.org.uk/registeryourinterest/CreateAccount.aspx" target="_blank">You can register your interest here</a>.</p>',
                },
                {
                    questionNumber: 2, //* Q7
                    evQuestion: ['nglp', 'ndp'],
                    questionIsActive: false,
                    lastQuestion: true,
                    subQuestionRequired: true,
                    preText: '',
                    question: '<p>Have you been a resident in the UK for the three years prior to submitting your application?</p>',
                    answer: 'yes',
                    userAnswer: '',
                    questionErrorSet: false,
                    required: true,
                    questionErrorPopup: false,
                    questionPopupOn: [],
                    questionCorrectPopupMessage: '',
                    questionErrorPopupMessage: '',
                    errorMessage: '',


                    subQuestionNumber: 2, //* Q7a
                    subQuestionIsActive: true,
                    subPreText: '',
                    subQuestion: '<p>Do any of the following reasons for not having lived in the UK for the last three years apply to you?</p><ul><li>I have served abroad with the British armed forces or on UK Government Service</li><li>I have spent a year abroad as part of my UK undergraduate degree</li><li>I have spent less than six months abroad</li><li>I have spent up to a year abroad on (extended) holiday(s)</li></ul>',
                    subAnswer: 'yes',
                    subUserAnswer: '',
                    subQuestionErrorSet: false,
                    subRequired: true,
                    subQuestionErrorPopup: true,
                    subQuestionPopupOn: ['error'],
                    subQuestionCorrectPopupMessage: '',
                    subQuestionErrorPopupMessage: '<p>The police need to ensure that adequate and satisfactory vetting is undertaken for all applicants and require at least a three-year checkable history in order to do this. Therefore, it is a requirement to have been a resident of the UK for at least three years, to allow for the necessary vetting enquiries to take place. You can find more in-depth detail about vetting and whether your circumstances would make you eligible here.</p>',
                    subErrorMessage: '<h4>The police need to ensure that adequate and satisfactory vetting is undertaken for all applicants and require at least a three-year checkable history in order to do this. Therefore, it is a requirement to have been a resident of the UK for at least three years, to allow for the necessary vetting enquiries to take place. You can find more in-depth detail about vetting and whether your circumstances would make you eligible here.</p>',
                }]
            },
            {
                sectionNumber: 5,
                sectionComplete: false,
                sectionTitle: 'Political affiliation',
                sectionIntro: '',
                sectionSkipped: false,
                questions: [{
                    questionNumber: 1, //* Q8
                    evQuestion: ['nglp', 'ndp'],
                    questionIsActive: true,
                    lastQuestion: false,
                    subQuestionRequired: false,
                    preText: '',
                    question: '<p>Are you, or have you previously been a member of an organisation whose constitution, aims, objectives or pronouncements may contradict the duty to promote racial and religious equality?</p>',
                    answer: 'no',
                    userAnswer: '',
                    questionErrorSet: false,
                    required: true,
                    questionErrorPopup: true,
                    questionPopupOn: ['error'],
                    questionCorrectPopupMessage: '',
                    questionErrorPopupMessage: '<p>As a serving police officer you have a duty to promote racial, religious and social equality. Police officers must be impartial and objective when performing their duties with the public. If you’re a member of an extreme political group you won’t be eligible for the programme.</p><p>If you are unsure, please <a href="mailto:selection@policenow.org.uk" target="_blank">contact us</a> to discuss in more detail.</p>',
                    errorMessage: '<p>As a serving police officer you have a duty to promote racial, religious and social equality. Police officers must be impartial and objective when performing their duties with the public. If you’re a member of an extreme political group you won’t be eligible for the programme.</p><p>If you are unsure, please <a href="mailto:selection@policenow.org.uk" target="_blank">contact us</a> to discuss in more detail.</p>'
                },
                {
                    questionNumber: 2, //* Q9
                    evQuestion: ['nglp', 'ndp'],
                    questionIsActive: false,
                    lastQuestion: true,
                    subQuestionRequired: false,
                    preText: '',
                    question: '<p>The Home Office requires you do not have an active role in politics whilst serving as a police officer.</p><p>Do you plan to take an active role in politics once appointed as a police officer?</p>',
                    answer: 'no',
                    userAnswer: '',
                    questionErrorSet: false,
                    required: true,
                    questionErrorPopup: true,
                    questionPopupOn: ['error'],
                    questionCorrectPopupMessage: '',
                    questionErrorPopupMessage: '<p>As a serving police officer you are able to vote during political elections and referendums however you cannot campaign, fund or advertise any political party or organisation whilst serving as a police officer. You may wish to continue to apply so the details of your circumstances can be reviewed by our recruitment team.</p>',
                    errorMessage: '<p>As a serving police officer you are able to vote during political elections and referendums however you cannot campaign, fund or advertise any political party or organisation whilst serving as a police officer. You may wish to continue to apply so the details of your circumstances can be reviewed by our recruitment team.</p>'
                }]
            },
            {
                sectionNumber: 6,
                sectionComplete: false,
                sectionTitle: 'Finance',
                sectionIntro: '<p>Police officers are in a privileged position with regards to access to information and could be considered potentially vulnerable to corruption. Applicants should therefore not be under pressure from undischarged debts or liabilities and should be able to manage loans and debts sensibly.</p>',
                sectionSkipped: false,
                questions: [{
                    questionNumber: 1, //* Q10
                    evQuestion: ['nglp', 'ndp'],
                    questionIsActive: true,
                    lastQuestion: false,
                    subQuestionRequired: false,
                    preText: '',
                    question: '<p>Are you currently registered as bankrupt or have you been registered as bankrupt within the last three years?</p>',
                    answer: 'no',
                    userAnswer: '',
                    questionErrorSet: false,
                    required: true,
                    questionErrorPopup: false,
                    questionPopupOn: [],
                    questionCorrectPopupMessage: '',
                    questionErrorPopupMessage: '<p>Police officers are in a privileged position with regards to access to information and could be considered potentially vulnerable to corruption. Applicants should therefore not be under pressure from undischarged debts or liabilities and should be able to manage loans and debts sensibly.To be eligible to join the police service, you cannot have been bankrupt within the last three years.</p><p>However, once three years have passed, you will be eligible to apply. Please <a href="https://careers.policenow.org.uk/registeryourinterest/CreateAccount.aspx" target="_blank">register your interest</a> for a future programme intake here.</p>',
                    errorMessage: '<p>Police officers are in a privileged position with regards to access to information and could be considered potentially vulnerable to corruption. Applicants should therefore not be under pressure from undischarged debts or liabilities and should be able to manage loans and debts sensibly.To be eligible to join the police service, you cannot have been bankrupt within the last three years.</p><p>However, once three years have passed, you will be eligible to apply. Please <a href="https://careers.policenow.org.uk/registeryourinterest/CreateAccount.aspx" target="_blank">register your interest</a> for a future programme intake here.</p>'
                },
                {
                    questionNumber: 2, //* Q11
                    evQuestion: ['nglp', 'ndp'],
                    questionIsActive: false,
                    lastQuestion: true,
                    subQuestionRequired: false,
                    preText: '',
                    question: '<p>Do you currently have a County Court Judgement (CCJ) registered against you?</p>',
                    answer: 'no',
                    userAnswer: '',
                    questionErrorSet: false,
                    required: true,
                    questionErrorPopup: true,
                    questionPopupOn: ['error'],
                    questionCorrectPopupMessage: '',
                    questionErrorPopupMessage: '<p>To be eligible to join the police service, you cannot have a <strong><u>current</u></strong> County Court Judgement (CCJ).</p><p>Once you no longer have a current CCJ, you will be eligible to apply. <a href="https://careers.policenow.org.uk/registeryourinterest/CreateAccount.aspx" target="_blank">Please register your interest for a future programme intake here</a>.</p>',
                    errorMessage: '<p>To be eligible to join the police service, you cannot have a <strong><u>current</u></strong> County Court Judgement (CCJ).</p><p>Once you no longer have a current CCJ, you will be eligible to apply. <a href="https://careers.policenow.org.uk/registeryourinterest/CreateAccount.aspx" target="_blank">Please register your interest for a future programme intake here</a>.</p>'
                }]
            }
        ]


    },
    getters: {
        gChoice(state) {
            return state.eligibilityVersion;
        },
        gSectionNumbers(state) {
            return state.sectionNumbers;
        },
        gChoiceOnlyFor(state){
            return state.eligibilityVersion;
        },
        gActiveSection(state) {
            return state.activeSection;
        },
        gSectionStop(state) {
            return state.sectionStop;
        },
        gErrors(state) {
            // Get the array
            const errorStateSetOnQuestionNumbers = state.errorStateSetOnQuestionNumbers
            // If the error array is undefined or empty, then we have no errors
            if (errorStateSetOnQuestionNumbers === undefined || errorStateSetOnQuestionNumbers.length == 0) {
                return false // No errors
            } else {
                return true // We have errors
            }


            // return state.errorState
        },
        gSections(state) {
            return state.sections;
        },
        gTotalSections(state) {
            return state.totalSteps;
        },
        // Pass Custom Arguments to Vuex Getters
        // https://www.smashingmagazine.com/2020/01/data-components-vue-js/
        gIsCurrentSectionComplete: (state) => (sectionNumber) => {
            // Find the section by using the passed section number
            const section = state.sections.find(section => section.sectionNumber === sectionNumber)

            return section.sectionComplete;
        },
        gIsPreviousQuestionAnswered: (state) => (sectionNumber, questionNumber) => {
            // Find the section
            const section = state.sections.find(section => section.sectionNumber === sectionNumber)

            // Find the question by using the section above
            const question = section.questions.find(question => question.questionNumber === questionNumber)


            return question.userAnswer;
        },
        // Pass Custom Arguments to Vuex Getters
        // https://www.smashingmagazine.com/2020/01/data-components-vue-js/
        gCurrentQuestionErrorPopupStatus: (state) => (sectionNumber, questionNumber) => {
            // Find the section
            const section = state.sections.find(section => section.sectionNumber === sectionNumber)

            // Find the question by using the section above
            const question = section.questions.find(question => question.questionNumber === questionNumber)

            // Get the error popup state
            return question.questionErrorPopup;
        },
        gCurrentSubQuestionErrorPopupStatus: (state) => (sectionNumber, questionNumber) => {
            // Find the section
            const section = state.sections.find(section => section.sectionNumber === sectionNumber)

            // Find the question by using the section above
            const question = section.questions.find(question => question.questionNumber === questionNumber)

            // Get the details to when to show the sub question popup messages
            return question.subQuestionErrorPopup;
        },
        // Pass Custom Arguments to Vuex Getters
        // https://www.smashingmagazine.com/2020/01/data-components-vue-js/
        gCurrentQuestionErrorPopupMessage: (state) => (sectionNumber, questionNumber) => {
            // Find the section
            const section = state.sections.find(section => section.sectionNumber === sectionNumber)

            // Find the question by using the section above
            const question = section.questions.find(question => question.questionNumber === questionNumber)

            // Get the 'error' answer popup message
            return question.questionErrorPopupMessage;
        },
        gCurrentQuestionCorrectPopupMessage: (state) => (sectionNumber, questionNumber) => {
            // Find the section
            const section = state.sections.find(section => section.sectionNumber === sectionNumber)

            // Find the question by using the section above
            const question = section.questions.find(question => question.questionNumber === questionNumber)

            // Get the 'correct' answer popup message
            return question.questionCorrectPopupMessage;
        },
        gCurrentQuestionquestionPopupOn: (state) => (sectionNumber, questionNumber) => {
            // Find the section
            const section = state.sections.find(section => section.sectionNumber === sectionNumber)

            // Find the question by using the section above
            const question = section.questions.find(question => question.questionNumber === questionNumber)

            // Get the details to when to show the popup messages
            return question.questionPopupOn;
        },
        gCurrentSubQuestionquestionPopupOn: (state) => (sectionNumber, subQuestionNumber) => {
            // Find the section
            const section = state.sections.find(section => section.sectionNumber === sectionNumber)

            // Find the question by using the section above
            const question = section.questions.find(question => question.questionNumber === subQuestionNumber)

            // Get the details to when to show the popup messages
            return question.subQuestionPopupOn;
        },
        gCurrentSubQuestionCorrectPopupMessage: (state) => (sectionNumber, subQuestionNumber) => {
            // Find the section
            const section = state.sections.find(section => section.sectionNumber === sectionNumber)

            // Find the question by using the section above
            const question = section.questions.find(question => question.questionNumber === subQuestionNumber)

            // Get the 'correct' sub answer popup message
            return question.subQuestionCorrectPopupMessage;
        },
        gCurrentSubQuestionErrorPopupMessage: (state) => (sectionNumber, questionNumber) => {
            // Find the section
            const section = state.sections.find(section => section.sectionNumber === sectionNumber)

            // Find the question by using the section above
            const question = section.questions.find(question => question.questionNumber === questionNumber)

            // Get the 'error' sub answer popup message
            return question.subQuestionErrorPopupMessage;
        }
    },
    mutations: {

        mStartChoice(state, {choice}) {
            // console.log('M choice = '+choice)
            state.eligibilityVersion = choice
            state.activeSection++
        },

        mSectionStop(state) {
            state.sectionStop = true;
        },

        mSetErrorState(state, errorState) {
            // set the error starte
            // see ErrorCentre.vue, mounted()
            state.errorState = errorState
        },

        // Update the question's "user answer"
        // { sectionNumber, questionNumber, userAnswer } payload is passed from Answer.vue
        mUserAnswer(state, {
            sectionNumber,
            questionNumber,
            userAnswer,
            subQuestionNumber,
            //subQuestionRequired,
            questionErrorSet
        }) {
            // Find the section
            const section = state.sections.find(section => section.sectionNumber === sectionNumber)

            // Find the question by using the section above
            const question = section.questions.find(question => question.questionNumber === questionNumber)

            // Update the user answer
            question.userAnswer = userAnswer

            // Update the question error set value (is this a wrong answer?)
            question.questionErrorSet = questionErrorSet

            //! For setting & unsetting the errors
            //* If the answer is WRONG
            if ( userAnswer != question.answer ) {
                // Add the section number and question number to the error array
                state.errorStateSetOnQuestionNumbers.push('S'+sectionNumber+'Q'+questionNumber)

                // Set the error state to show an error
                state.errorState = true
            }
            //* Else if the answer is RIGHT
            if ( userAnswer == question.answer ) {

                //! RIGHT HERE IS AN ISSUE
                //? this will clear all wrong answers and not just the sub question
                // If there is a sub question
                //if ( subQuestionRequired == true ) {

                    // This checks to remove the error IF the user changes their mind on the parent question and negate the need for a sub answer, so we remove the error on the sub (if need)

                    let errorStateSetOnQuestionNumbers = state.errorStateSetOnQuestionNumbers

                    //let errorStateSetOnQuestionNumbersIndex = errorStateSetOnQuestionNumbers.indexOf('S'+sectionNumber+'Q'+questionNumber+'Sub'+subQuestionNumber);

                    // Remove the section number and subquestion number from the error array
                    // errorStateSetOnQuestionNumbers.splice(errorStateSetOnQuestionNumbersIndex, 1);
                    // errorStateSetOnQuestionNumbers.filter(function(el){return el !== 'S'+sectionNumber+'PQ'+questionNumber+'Sub'+subQuestionNumber});

                    for (var i = errorStateSetOnQuestionNumbers.length; i--;) {
                        if (errorStateSetOnQuestionNumbers[i] === 'S'+sectionNumber+'PQ'+questionNumber+'Sub'+subQuestionNumber) errorStateSetOnQuestionNumbers.splice(i, 1);
                    }
                    console.log('From Question S'+sectionNumber+'PQ'+questionNumber+'Sub'+subQuestionNumber)

                //}



                //* If the error array includes the section number and question number
                if ( state.errorStateSetOnQuestionNumbers.includes('S'+sectionNumber+'Q'+questionNumber ) ) {
                    let errorStateSetOnQuestionNumbers = state.errorStateSetOnQuestionNumbers

                    let errorStateSetOnQuestionNumbersIndex = errorStateSetOnQuestionNumbers.indexOf('S'+sectionNumber+'Q'+questionNumber);

                    // Set the error state to NOT show an error
                    state.errorState = false

                    // Remove the section number and question number from the error array
                    errorStateSetOnQuestionNumbers.splice(errorStateSetOnQuestionNumbersIndex, 1);

                    // console.log( 'includes: '+'S'+sectionNumber+'Q'+questionNumber )
                }

            }
        },
        // Update the question's "subuser answer"
        mUserSubAnswer(state, {
            sectionNumber,
            parentQuestionNumber,
            subQuestionNumber,
            subUserAnswer,
            questionErrorSet
        }) {
            // Find the section
            const section = state.sections.find(section => section.sectionNumber === sectionNumber)

            // Find the question by using the section above
            const question = section.questions.find(question => question.subQuestionNumber === subQuestionNumber)

            // Find parent question
            const questionParent = section.questions.find(question => question.questionNumber === parentQuestionNumber)

            console.log( 'Set from sub question - Parent question number: ' + parentQuestionNumber )

            // Update the user answer
            question.subUserAnswer = subUserAnswer

            //! For setting & unsetting the errors
            //* If the answer is WRONG
            if ( subUserAnswer != question.subAnswer ) {
                // Add the section number and subquestion number to the error array
                state.errorStateSetOnQuestionNumbers.push('S'+sectionNumber+'PQ'+parentQuestionNumber+'Sub'+subQuestionNumber)

                // Set the error state to show an error
                state.errorState = true
            }
            //* Else if the answer is RIGHT & the error array includes the section number and question number
            if ( subUserAnswer == question.subAnswer && state.errorStateSetOnQuestionNumbers.includes('S'+sectionNumber+'PQ'+parentQuestionNumber+'Sub'+subQuestionNumber )) {

                let errorStateSetOnQuestionNumbers = state.errorStateSetOnQuestionNumbers

                // let errorStateSetOnQuestionNumbersIndex = errorStateSetOnQuestionNumbers.indexOf('S'+sectionNumber+'PQ'+parentQuestionNumber+'Sub'+subQuestionNumber);

                // Set the error state to NOT show an error
                state.errorState = false

                // Remove the section number and question number from the error array
                // errorStateSetOnQuestionNumbers.splice(errorStateSetOnQuestionNumbersIndex, 1);
                // errorStateSetOnQuestionNumbers.filter(function(el){return el !== 'S'+sectionNumber+'PQ'+parentQuestionNumber+'Sub'+subQuestionNumber});

                for (var i = errorStateSetOnQuestionNumbers.length; i--;) {
                    if (errorStateSetOnQuestionNumbers[i] === 'S'+sectionNumber+'PQ'+parentQuestionNumber+'Sub'+subQuestionNumber) errorStateSetOnQuestionNumbers.splice(i, 1);
                }

                console.log('From Sub Question S'+sectionNumber+'PQ'+parentQuestionNumber+'Sub'+subQuestionNumber)
            }

            //* If the answer is RIGHT
            //* For sub questions reset error state, if the answer undoes the previous wrong answer
            if ( question.subAnswer == subUserAnswer ) {
                // alert()
                state.errorState = false
                questionParent.questionErrorSet = false

                console.log( 'Parent question number: ' + questionParent + ' has been set to: ' + questionParent.questionErrorSet )

                questionParent




                // For unsetting the error messages, if the sub questions correct answer undoes the parents wrong answer
                let errorStateSetOnQuestionNumbers = state.errorStateSetOnQuestionNumbers

                let errorStateSetOnQuestionNumbersIndex = errorStateSetOnQuestionNumbers.indexOf('S'+sectionNumber+'PQ'+parentQuestionNumber+'Sub'+subQuestionNumber);

                // Set the error state to NOT show an error
                state.errorState = false

                // Remove the section number and question number from the error array
                errorStateSetOnQuestionNumbers.splice(errorStateSetOnQuestionNumbersIndex, 1);

                console.log( 'includes: '+'S'+sectionNumber+'PQ'+parentQuestionNumber+'Sub'+subQuestionNumber )
            }

            // Update the question error set value (is this a wrong answer?)
            question.subQuestionErrorSet = questionErrorSet

            // If error is set at a question level
            // Also set the error in the error centre
            // TODO:
            // I think the logic will need work here to allow changing of mind on an answer
            // if (questionErrorSet === true) {
            //     state.errorState = true
            // }
        },

        mNextQuestionIsActive(state, {
            sectionNumber,
            questionNumber,
            subQuestionRequired
        }) {
            // Find the section
            const section = state.sections.find(section => section.sectionNumber === sectionNumber)

            // const question = section.questions.find(question => question.questionNumber === questionNumber)

            // Find the NEXT question by using the section above
            // Then add one to select the next question
            const nextQuestion = section.questions.find(question => question.questionNumber === questionNumber + 1)

            // if ( question.userAnswer == question.answer ) {
            //   //
            //   question.subQuestionRequired = false
            // }

            // If THIS question doesn't doesn't require a sub question
            if (!subQuestionRequired && nextQuestion != null) {
                // Set the next question to active
                nextQuestion.questionIsActive = true // Activate next question (if needed)
            }
        },

        // Mark section as complete
        mSectionComplete(state, {
            sectionNumber
        }) {
            // Find the section
            const section = state.sections.find(section => section.sectionNumber === sectionNumber)

            section.sectionComplete = true
        },

        // For previous section
        mActiveSectionMinusOne(state) {
            state.activeSection--
        },

        // For next section
        mActiveSectionAddOne(state) {
            state.activeSection++
        },

        // Used for setting the DOM is rendered
        mSummariesActive(state) {
            state.summariesActive = true
        },

        // If user says they don't want to continue, skip to the summaries
        mSkipToSummaries(state) {
            const totalSteps = state.totalSteps // Get the total number of steps

            state.activeSection = totalSteps + 1 // 1 more than total steps
        }
    },
    actions: {},
    modules: {}
})