import { createApp } from 'vue'
import App from './App.vue'
import VueGtag from "vue-gtag-next";
import store from './store'

const app = createApp(App);

//* For Google Analytics
// Repo - https://github.com/MatteoGabriele/vue-gtag
// Docs - https://matteo-gabriele.gitbook.io/vue-gtag/v/next/
app.use(VueGtag, {
  property: {
    id: "GTM-5H7SKFC"
  }
  //* For debugging gtag analytic events
  // ,useDebugger: true
});

app.use(store);
app.mount("#app");
